# REST API for uauto backend

## users

#### Backend JSON Structure

```
    id        int 
    CreatedAt timestamp with timezone
    UpdatedAt timestamp with timezone
    DeletedAt timestamp with timezone
    Mail      string
    Username  string
    Password  string
    Mobile    string
    U_Type      int
    Name      string
    License   string
    StandID   int  ForeignKey
    Vehicles  []int VehicleID  ForeignKey
    Address   string

    // Types explaination
    // 0 passenger
    // 1 driver
    // 2 union member
    // 3 driver + union member
    // 4 root

```
**/login/ - POST**
        request payload - {"mail":"", "mobile":"", "password":""}

        If email and password match response - 200
        response format {"success": true, "user": {}}
        
        If they don't match response - 205 (Reset Content Header)
        response format {"success": false, "reason": ""}



**/users/ - GET - Full list**
        response format {"success": true, "users": []}

**/users/ - POST - Create**

        request payload = {"name":"", "mobile":"", "mail":"", "password":"", "type":0, "license":null, "address":""}


        If created successfully status code - 201
        response format {"success": true, "user": <user_id: int>}

/users/{id} - GET - Info of record at {id}

/users/{id} - DELETE - Delete record at {id}

## vehicles

#### Backend JSON Structure
```
   
    ID        int 
    CreatedAt timestamp with timezone
    UpdatedAt timestamp with timezone
    DeletedAt timestamp with timezone 
	UserID       int         ForeignKey
	RegNumber    string
	Manufacturer string
    V_Model        string
    FullCheck    boolean
    Ownership    int UserID 
```
/vehicles/ - GET - Full list

/vehicles/ - POST - Create

/vehicles/{id} - GET - Info of record at {id}

/vehicles/{id} - DELETE - Delete record at {id}

## stands

#### Backend JSON Structure

```
    
    ID        int 
    CreatedAt timestamp with timezone
    UpdatedAt timestamp with timezone
    DeletedAt timestamp with timezone
    Name  string
    Users []int UserID ForeignKey

```

/stands/ - GET - Full list

/stands/ - POST - Create

/stands/{id} - GET - Info of record at {id}

/stands/{id} - DELETE - Delete record at {id}

## rides
```
    ID        int 
    CreatedAt timestamp with timezone
    UpdatedAt timestamp with timezone
    DeletedAt timestamp with timezone
   
```
/rides/ - GET - Full list

/rides/ - POST - Create

/rides/{id} - GET - Info of record at {id}

/rides/{id} - DELETE - Delete record at {id}


